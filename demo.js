module.exports = function() {
	var faker = require('faker');
	var _ = require('lodash');

	function randomNum(min, max) {
		return faker.random.number({
			min: min,
			max: max
		});
	}

	var classes = ['fa-hospital-o', 'fa-heartbeat', 'fa-file-o'];
	var textPatterns = ['Han nacido # becerros', '# toros han muerto', 'Hay # toros solicitudes de cesión'];
	var numbers = [randomNum(15, 25), randomNum(1, 10), randomNum(10, 15)];

	return {
		Messages: _.times(40, function(n) {
			return {
				id: n,
				title: faker.lorem.sentence(),
				date: faker.date.recent()
			}
		}),
		Notifications: _.times(3, function(n) {
			return {
				id: n,
				class: classes[n],
				amount: numbers[n],
				lastDate: faker.date.recent(),
				textPattern: textPatterns[n]
			}
		}),
		Groups: _.times(7, function(n) {
			var name = faker.lorem.words(2).join(' ');
			return {
				id: n,
				name: name,
				elements: _.times(randomNum(3, 8), function(e) {
					return {
						name: name + ' ' + randomNum(1, 100),
						link: 'empty'
					}
				})
			}
		}),
		Animals: _.times(10, function(n) {
			var sexo = n < 5 ? 'M' : 'H';
			var zeros = '000';
			return {
				sexo: sexo,
				cod_gen: 'EAA0' + randomNum(15, 98) + sexo + (zeros + n).slice(-3)
			}	
		})
	};
}