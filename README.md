JSON GENERATOR PROJECT
----------------------

# INSTALLATION

1. install globally json-server package:
`npm install -g json-server
2. execute the following command:
`npm install

# RUN SERVER
1. execute the following command:
`json-server demo.json

> NOTE: DO NOT MODIFY the demo.js. Clone it and work with the new file.